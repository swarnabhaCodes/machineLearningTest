package com.swarnabha.ml.main;

import java.io.File;
import java.io.IOException;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

public class EvaluateRecommender {

  public static void main(String[] args) throws IOException, TasteException {
    // TODO Auto-generated method stub
    DataModel model = new FileDataModel(new File("/Users/swarnabha_lahiri/Server/hadoop-2.7.0/DataFiles/PersonRecommendation.csv"));
    RecommenderEvaluator evaluator = new AverageAbsoluteDifferenceRecommenderEvaluator();
    RecommenderBuilder builder = new MyRecommenderBuilder();
    double result = evaluator.evaluate(builder, null, model, 0.9, 1.0);
    System.out.println(result);
  }

  
  private static class MyRecommenderBuilder implements RecommenderBuilder{

    @Override
    public Recommender buildRecommender(DataModel dataModel) throws TasteException {
      // TODO Auto-generated method stub
      UserSimilarity similarity = new PearsonCorrelationSimilarity(dataModel);
      UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, dataModel);
      return new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
    }
    
  }
}
